package com.prashantgupta.travelkhanaassignment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Psych Inc on 18-10-2015.
 */
public class OrderDetails extends Activity{

    TextView orderInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_order);

        String station = getIntent().getStringExtra("Station");

        orderInfo = (TextView)findViewById(R.id.order_info);

        orderInfo.setText("Restaurant:" + "\n" + "Your order:" + "\n" + "Total Amount:" + "\n" + "Payment Method:" + "\n" + "Delivery Time:" + "\n" + "Delivery Station: " + station);


    }

    public void goBack(View v){

        this.finish();
    }
}
