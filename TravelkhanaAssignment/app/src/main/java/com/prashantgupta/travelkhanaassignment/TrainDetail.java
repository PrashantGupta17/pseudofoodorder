package com.prashantgupta.travelkhanaassignment;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Psych Inc on 18-10-2015.
 */
public class TrainDetail {

    @SerializedName("station_name")
    private String stationName;

    @SerializedName("latitude")
    private String stationLat;

    @SerializedName("longitude")
    private String stationLng;

    @SerializedName("is_stop")
    private String stationStoppage;

    @SerializedName("station_code")
    private String stationCode;

    public String getStationName() {
        return stationName;
    }

    public String getStationLat() {
        return stationLat;
    }

    public String getStationLng() {
        return stationLng;
    }

    public String getStationStoppage() {
        return stationStoppage;
    }

    public String getStationCode() {
        return stationCode;
    }
}
