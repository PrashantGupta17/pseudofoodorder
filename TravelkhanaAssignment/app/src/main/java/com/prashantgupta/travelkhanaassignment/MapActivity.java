package com.prashantgupta.travelkhanaassignment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.plus.Plus;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Psych Inc on 18-10-2015.
 */
public class MapActivity extends AppCompatActivity {

    GoogleMap simpleMap;
    private final String trainURL = "http://184.106.215.147/travelkhana/services/TrainSchedule?trainno=11015&login=testapi:testapi";
    ArrayList<TrainDetail> allStops;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.train_route);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        getSupportActionBar().setTitle("Route");

        mContext = this;

        simpleMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

        simpleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        simpleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {

                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.info_window, null);

                // Getting reference to the TextView to set latitude
                TextView tvName = (TextView) v.findViewById(R.id.info_text);

                tvName.setText(arg0.getTitle() + "(" + arg0.getSnippet() + ")");

                return v;

            }
        });

        simpleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                Intent i = new Intent(mContext, OrderDetails.class);
                i.putExtra("Station", marker.getTitle());
                startActivity(i);

            }
        });

        allStops = new ArrayList<>();

        getTrainStations(trainURL);

    }

    private void getTrainStations(String url) {

        JsonObjectRequest jsonRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // the response is already constructed as a JSONObject!

                        try{

                            Gson gson = new Gson();

                            allStops = gson.fromJson(response.toString(), Train.class).getTrain_info();

                        }catch(IllegalStateException e){
                        }

                        markTrainRoute(allStops);

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        Volley.newRequestQueue(this).add(jsonRequest);

    }

    private void markTrainRoute(ArrayList<TrainDetail> allStops) {

        if(allStops != null){

            PolylineOptions options = new PolylineOptions();
            options.color( Color.parseColor("#CC0000FF") );
            options.width( 5 );
            options.visible( true );

            ArrayList<MarkerOptions> mps = new ArrayList<>();

            for (TrainDetail stop : allStops )
            {
                if("NA".equalsIgnoreCase(stop.getStationLat()) || "NA".equalsIgnoreCase(stop.getStationLng())){

                    continue;
                }

                LatLng ltLn = new LatLng(Double.valueOf(stop.getStationLat()), Double.valueOf(stop.getStationLng()));

                if(stop.getStationStoppage().equalsIgnoreCase(String.valueOf(1))){

                    MarkerOptions mp = new MarkerOptions();
                    mp.position(ltLn);
                    mp.title(stop.getStationName());
                    mp.snippet(stop.getStationCode());
                    mps.add(mp);
                }

                options.add(ltLn);

            }

            simpleMap.addPolyline(options);

            for(MarkerOptions mp : mps){

                simpleMap.addMarker(mp);

            }

            MarkerOptions zoomLoc = mps.get(10);

            simpleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(zoomLoc.getPosition(), 4));

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (MainActivity.mGoogleApiClient.isConnected()) {


                Plus.AccountApi.clearDefaultAccount(MainActivity.mGoogleApiClient);
                Plus.AccountApi.revokeAccessAndDisconnect(MainActivity.mGoogleApiClient);
                MainActivity.mGoogleApiClient.disconnect();
            }

            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
